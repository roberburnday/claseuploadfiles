<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\UploadedFiles;

class FilesController extends Controller
{

    /**
     * Almacena un solo fichero
     * (Request = $_POST + $_GET + $_FILES)
     * @param Request $request Petición donde almacenamos las imagenes 
     */
    public function uploadSingleFile(Request $request){
        //TODO: falta un validator pero me lo salto porque es para la clase 
        //--> el request debería estar comprobado por un validator
        DB::beginTransaction();
        try{
            $uploadFile = new UploadedFiles();
            $uploadFile->name = $request->name;
            $folderName = "chimichangas";
            //cualquier otro valor único vale --> el id del fichero, un unicID, etc
            $realNameFile = date('U').".".str_replace(' ','_',$request->file('fileToSave')->getClientOriginalName());
            $urlPath = 'storage/files/'.$folderName;
            //empezamos el upload del fichero
            $uploadFile->path = $request->fileToSave->storeAs(
                $folderName, 
                $realNameFile
            );
            //seteamos los valores al modelo
            $uploadFile->url = url($urlPath.'/'.$realNameFile);
            $uploadFile->enable = $request->enable;
            //guardamos el fichero
            $resultSave = $uploadFile->save();
            //commiteamos y retornamos ok
            DB::commit();
            return response(['file'=>$uploadFile->toArray()],200);
        }catch(\Exception $e){
            DB::rollBack();
            return response(['error'=>$e],500);
        }
    }

    /**
     * 
     */
    public function uploadMultipleFile(Request $request){
        //TODO: habría que preparar el resto de la function pero es por ejemplificar
        $folderName = "burritos";
        $urlPath = 'storage/files/'.$folderName;
        foreach($request->file('filesToSave') as $index => $file){
            // do uploading like what you are doing in your single file.
            $realNameFile = date('U').$index.".".str_replace(' ','_',$file->getClientOriginalName());
            $path = $file->storeAs(
                $folderName, 
                $realNameFile
            );
        }
    }
}
