<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FilesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('uploadFiles',[FilesController::class, 'getFiles'])->name('files.get');
Route::post('multipleFiles',[FilesController::class, 'uploadMultipleFile'])->name('files.multiple');
Route::post('singleFile',[FilesController::class, 'uploadSingleFile'])->name('files.single');
