<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <title>Form File Upload</title>

        <script
  src="https://code.jquery.com/jquery-3.6.0.slim.js"
  integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY="
  crossorigin="anonymous"></script>

    </head>
    <body>
        <h1>Hola hola formulario files</h1>
        <h6>Form single files</h6>
        <form>
            <input name="name"> <br>
            <input type="checkbox" name="enable" value="true"> Enable <br>
            <input type="file" name="fileToSave"> <br>
        </form>
        <button class="sendSingleFile">Send</button>
        <hr style="border: 1px solid black">
        <h6>Form multiple files</h6>
        <form>
            <input name="name"> <br>
            <input type="checkbox" name="enable" value="true"> Enable <br>
            <input type="file" name="filesToSave"> <br>
        </form>
        <button class="sendMultipleFile">Send</button>
    </body>

    

    <script>
        $(document).ready(function()
        {
            $('.sendSingleFile').on('click', function()
            {
                alert('clicked single!');
            });
            $('.sendMultipleFile').on('click', function()
            {
                alert('clicked multiple!');
            });
        });
        document.onclick = function(e) {
            e.preventDefault();
        }
        //----------------------------------- componente function --------------------------------
        sendSingleFile(event){
            alert("sending single file");
        }

        sendMultipleFile(event){
            alert("sending multiple file");
        }

        //-------------------- funciones del mixin ------------------------------------------------

    </script>
</html>
