<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadedFiles extends Model
{
    use HasFactory;

    protected $table = 'uploaded_files';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name',
        'url',
        'path',
        'enable'
    ]; 
    
    //UploadedFiles::fill(['name' => 'Amsterdam to Frankfurt', 'url'=> 'xxxxxx', 'falseField'=>'value false']);
    //UploadedFiles::fill($request->toArray());
}
